﻿using System.Xml.Linq;

namespace Encode64.Models.NewFolder
{
    public class DtoResponse
    {
        private string input;
        public string Input { get { return input; } set { input = value; } }
    }
}
