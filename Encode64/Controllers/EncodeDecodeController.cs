﻿using Encode64.Models;
using Encode64.Models.NewFolder;
using Encode64.NewFolder;
using Encode64.Services;
using Microsoft.AspNetCore.Mvc;

namespace Encode64.Controllers
{
    [ApiController]
    [Route("api/v1/base64")]
    public class EncodeDecodeController : ControllerBase
    {
        private readonly IDecode decode = new Encode64Service();
        private readonly IEncode encode = new Encode64Service();

        /// <summary>
        /// Encode route will encode the data to base64.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("encode")]
        public DtoResponse EncodeData(Entry data)
        {
            string result = encode.Encode(data.Input);
            Console.WriteLine(result);
            DtoResponse response = new DtoResponse();
            response.Input = result;
            return response;
        }

        /// <summary>
        /// decode route will decode the data from base64.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("decode")]
        public DtoResponse DecodeData(Entry data)
        {
            string result = decode.Decode(data.Input);
            DtoResponse response = new DtoResponse();
            response.Input = result;
            return response;
        }
    }
}