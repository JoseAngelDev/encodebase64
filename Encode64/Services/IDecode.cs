﻿namespace Encode64.NewFolder
{
    public interface IDecode
    {
        string Decode (string data);
    }
}
