﻿
using Encode64.NewFolder;
using System.Text;

namespace Encode64.Services
{
    public class Encode64Service : IEncode, IDecode
    {
        public string Decode(string data)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(data));
        }

        public string Encode(string data)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(data));
        }
    }
}
