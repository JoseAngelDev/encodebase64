﻿using System.Text;

namespace EncodeLibrary
{
    public static class ControllerLogic
    {
        // Encode data to Base64.
        public static string EncodeData(string value)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(value));
        }

        // Decode data from Base64.
        public static string DecodeData(string encodedName)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(encodedName));
        }
    }
}