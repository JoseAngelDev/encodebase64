using Encode64.Models.NewFolder;
using Encode64.NewFolder;
using Encode64.Services;

namespace TestLogic
{
    [TestClass]
    public class TestController
    {
        [TestMethod]
        /// <summary>
        /// Test if the the data is encoded corectly.
        /// </summary>
        public void IsEncodedGood()
        {
            IEncode encode = new Encode64Service();
            string name = "Jose Angel";
            string encodedName = "Sm9zZSBBbmdlbA==";
            string result = encode.Encode(name);

            Assert.AreEqual(expected: encodedName, actual: result);
        }
 
        [TestMethod]
        /// <summary>
        /// Test if the the data is decoded corectly.
        /// </summary>
        public void IsDecodedGood()
        {
         IDecode decode = new Encode64Service();
            string name = "Jose Angel";
            string encodedName = "Sm9zZSBBbmdlbA==";
            string result = decode.Decode(encodedName);

            Assert.AreEqual(expected: name, actual: result);
        }
    }
}